﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;

namespace WinFormsGraphs
{
    public partial class Form1 : Form
    {
        private Bitmap drawArea;
        private const int vertexRadius = 15;
        private const int minimalVertexBetweenDistance = 3;
        private const int penSize = 3;
        private const int fontSize = 10;
        private float[] dashPattern = new float[] { 1, 3 };
        private FontFamily fontFamily;
        private Font font;
        private StringFormat format = new StringFormat();        
        private int saveCounter = 1;
        private ColorDialog colorDialog = new ColorDialog();
        Color activeVertexColor = Color.Black;
        private Vertex selectedVertex = null;
        private Graph G;
        private bool isVertexMoving = false;
        private Point lastMousePoint;



        public Form1()
        {
                    
            InitializeComponent();
            G = new Graph();
            drawArea = new Bitmap(Canvas.Size.Width, Canvas.Size.Height);
            Canvas.Image = drawArea;
            using(Graphics g = Graphics.FromImage(drawArea))
                g.Clear(Color.White);
            fontFamily = new FontFamily("Arial");
            font = new Font(fontFamily, fontSize);
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            label1.BackColor = activeVertexColor;
            KeyPreview = true;

            saveFileDialog1.Filter = "Graph files (*.graph)|*.graph";
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = "Graph1.graph";

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Graph files (*.graph)|*.graph";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;

            SetLanguage("pl-PL");
        }            

        private Vertex ClosestVertexInDistance(int x, int y, int distance)
        {
            Vertex closestVertex = null;
            double closestDistance = double.MaxValue;
            foreach (Vertex vertex in G.vertices)
            {
                double vertexDistance = Math.Sqrt(Math.Pow(vertex.X - x, 2) + Math.Pow(vertex.Y - y, 2));
                if (vertexDistance <= distance && vertexDistance <= closestDistance)
                {
                    closestVertex = vertex;
                    closestDistance = vertexDistance;
                }
            }
            return closestVertex;
        }

        private void DeleteSelected()
        {
            if (selectedVertex == null)
                return;

            List<Edge> neighborEdges = FindEdgesFrom(selectedVertex);
            foreach(Edge e in neighborEdges)
                G.edges.Remove(FindEdge(e.From, e.To));

            for (int i = selectedVertex.Index+1; i < G.vertices.Count; i++)
                G.vertices[i].Index--;

            G.vertices.Remove(selectedVertex);
            selectedVertex = null;
            DrawAll();
        }

        private void DrawVertex(Vertex vertex)
        {
            using (SolidBrush brush = new SolidBrush(Color.White))
            using (Pen pen = new Pen(vertex.Color, penSize))
            using (Graphics g = Graphics.FromImage(drawArea))
            {                
                g.FillEllipse(brush, vertex.X - vertexRadius, vertex.Y - vertexRadius, vertexRadius * 2, vertexRadius * 2);
                g.DrawEllipse(pen, vertex.X - vertexRadius, vertex.Y - vertexRadius, vertexRadius * 2, vertexRadius * 2);
                brush.Color = vertex.Color;
                g.DrawString((vertex.Index+1).ToString(), font, brush, new PointF(vertex.X, vertex.Y), format);
                if(vertex == selectedVertex)
                {
                    pen.DashPattern = dashPattern;
                    pen.Color = Color.White;
                    g.DrawEllipse(pen, vertex.X - vertexRadius, vertex.Y - vertexRadius, vertexRadius * 2, vertexRadius * 2);
                }
                var rect = new Rectangle(vertex.X - vertexRadius - minimalVertexBetweenDistance, vertex.Y - vertexRadius - minimalVertexBetweenDistance, 2 * (vertexRadius + minimalVertexBetweenDistance), 2 * (vertexRadius + minimalVertexBetweenDistance));
                Canvas.Invalidate(rect);
                Canvas.Update();
            }
        }


        private void DrawAll()
        {
            using (SolidBrush brush = new SolidBrush(Color.Red))
            using (Pen pen = new Pen(Color.Black, penSize))
            using (Pen dashedPen = new Pen(Color.White, penSize))
            using (Graphics g = Graphics.FromImage(drawArea))
            {
                dashedPen.DashPattern = dashPattern;
                g.Clear(Color.White);
                foreach(Edge e in G.edges)
                    g.DrawLine(pen, e.From.X, e.From.Y, e.To.X, e.To.Y);
                foreach(Vertex vertex in G.vertices)
                {
                    brush.Color = Color.White;
                    g.FillEllipse(brush, vertex.X - vertexRadius, vertex.Y - vertexRadius, vertexRadius * 2, vertexRadius * 2);
                    pen.Color = vertex.Color;
                    g.DrawEllipse(pen, vertex.X - vertexRadius, vertex.Y - vertexRadius, vertexRadius * 2, vertexRadius * 2);
                    brush.Color = vertex.Color;
                    g.DrawString((vertex.Index + 1).ToString(), font, brush, new PointF(vertex.X, vertex.Y), format);
                    if (vertex == selectedVertex)                    
                        g.DrawEllipse(dashedPen, vertex.X - vertexRadius, vertex.Y - vertexRadius, vertexRadius * 2, vertexRadius * 2);                                            
                }
                Canvas.Refresh();
            }
        }

        public Edge FindEdge(Vertex from, Vertex to)
        {
            foreach(Edge e in G.edges)
                if (e.From == from && e.To == to || e.From == to && e.To == from)
                    return e;
            return null;
        }
        public List<Edge> FindEdgesFrom(Vertex v)
        {
            List<Edge> foundEdges = new List<Edge>();
            foreach (Edge e in G.edges)
                if (e.From == v || e.To == v)
                    foundEdges.Add(e);
            return foundEdges;
        }

        private bool LoadGraph(Stream s)
        {
            using(StreamReader reader = new StreamReader(s))
            {
                Graph H = new Graph();
                string line;
                int index = 0;
                while(!reader.EndOfStream && (line = reader.ReadLine()) != "")
                {
                    Vertex v = new Vertex();
                    v.Index = index++;
                    int val;
                    string[] strips = line.Split(' ');
                    if(strips.Length != 3) return false;
                    if (int.TryParse(strips[0], out val) == false && val >= 0) return false;
                    v.X = val;
                    if (int.TryParse(strips[1], out val) == false && val >= 0) return false;
                    v.Y = val;
                    if (int.TryParse(strips[2], out val) == false) return false;
                    v.Color = Color.FromArgb(val);
                    H.vertices.Add(v);
                }
                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();
                    var strips = line.Split(' ');

                    if (strips.Length != 2) return false;
                    Edge e = new Edge();
                    int val;
                    if (int.TryParse(strips[0], out val) == false && val >= 0 && val < H.vertices.Count) return false;
                    e.From = H.vertices[val];
                    if (int.TryParse(strips[1], out val) == false && val >= 0 && val < H.vertices.Count) return false;
                    e.To = H.vertices[val];
                    H.edges.Add(e);
                }
                G = H;
            }
            
            s.Dispose();
            return true;
        }

        private bool SaveGraph(Stream s)
        {
            using (StreamWriter writer = new StreamWriter(s))
            {
                foreach (Vertex v in G.vertices)
                    writer.WriteLine($"{v.X} {v.Y} {v.Color.ToArgb()}");
                writer.WriteLine();
                foreach (Edge e in G.edges)
                    writer.WriteLine($"{e.From.Index} {e.To.Index}");
            }
            s.Dispose();
            return true;
        }

        private void Canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                if (selectedVertex != null)
                {
                    Vertex vertex = ClosestVertexInDistance(e.X, e.Y, vertexRadius + minimalVertexBetweenDistance);
                    if(vertex != null && vertex != selectedVertex)
                    {
                        Edge edge = FindEdge(selectedVertex, vertex);
                        if (edge == null)
                        {
                            edge = new Edge(selectedVertex, vertex);
                            G.edges.Add(edge);

                            DrawAll();
                        }
                        else
                        {
                            G.edges.Remove(edge);
                            DrawAll();
                        }
                        return;
                    }
                }
                if (ClosestVertexInDistance(e.X, e.Y, 2 * vertexRadius + minimalVertexBetweenDistance) != null)
                    return;
                Vertex newVertex = new Vertex(e.X, e.Y, activeVertexColor, G.vertices.Count);
                G.vertices.Add(newVertex);      
                DrawVertex(newVertex);
            }
            else if(e.Button == MouseButtons.Right)
            {
                Vertex oldSelectedVertex = selectedVertex;
                selectedVertex = ClosestVertexInDistance(e.X, e.Y, vertexRadius + minimalVertexBetweenDistance);
                    if (oldSelectedVertex != null)
                        DrawVertex(oldSelectedVertex);

                    if (selectedVertex != null)    
                    {
                        buttonDelVert.Enabled = true;
                        DrawVertex(selectedVertex);
                    }
                    else
                        buttonDelVert.Enabled = false;              
            }
            else if(e.Button == MouseButtons.Middle)
            {
                if(selectedVertex != null)
                {
                    isVertexMoving = true;
                    lastMousePoint = new Point(e.X, e.Y);
                }
            }    
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if(drawArea != null)
            {
                drawArea.Dispose();
                drawArea = new Bitmap(Canvas.Size.Width, Canvas.Size.Height);
                Canvas.Image = drawArea;

                DrawAll();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(colorDialog.ShowDialog() == DialogResult.OK)
            {
                activeVertexColor = colorDialog.Color;
                label1.BackColor = activeVertexColor;
                if (selectedVertex != null)
                {
                    selectedVertex.Color = activeVertexColor;
                    DrawVertex(selectedVertex);
                }
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            fontFamily.Dispose();
            drawArea.Dispose();
            font.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SetLanguage("pl-PL");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SetLanguage("en-001");
        }


        private void buttonDelVert_Click(object sender, EventArgs e)
        {
            DeleteSelected();
            buttonDelVert.Enabled = false;
        }

        private void buttonClearGraph_Click(object sender, EventArgs e)
        {
            G.vertices.Clear();
            G.edges.Clear();
            selectedVertex = null;
            DrawAll();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                e.Handled = true;
                DeleteSelected();
                buttonDelVert.Enabled = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = $"Graph{saveCounter}.graph";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SaveGraph(saveFileDialog1.OpenFile());                
                saveCounter++;                
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if(Path.GetExtension(openFileDialog1.FileName) != ".graph")
                {
                    string text = MessageBoxResource.TextExt;
                    string title= MessageBoxResource.Title;
                    MessageBox.Show(this, text,
                                   title, MessageBoxButtons.OK,
                                   MessageBoxIcon.Error,
                                   MessageBoxDefaultButton.Button1, 0);
                    return; 
                }
                if (!LoadGraph(openFileDialog1.OpenFile()))
                {
                    string text = MessageBoxResource.TextFail;
                    string title = MessageBoxResource.Title;
                    MessageBox.Show(this, text,
                                   title, MessageBoxButtons.OK,
                                   MessageBoxIcon.Error,
                                   MessageBoxDefaultButton.Button1, 0);
                    return;
                }
                selectedVertex = null;
                DrawAll();
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (isVertexMoving == false || selectedVertex == null)
                return;

            selectedVertex.X += e.X - lastMousePoint.X;
            selectedVertex.Y += e.Y - lastMousePoint.Y;            

            lastMousePoint = e.Location;
            DrawAll();
        }

        private void Canvas_MouseUp(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Middle && isVertexMoving == true)
            {
                isVertexMoving = false;
                if (selectedVertex.X < 0)
                    selectedVertex.X = 0;
                if (selectedVertex.X > Canvas.Width)
                    selectedVertex.X = Canvas.Width;
                if (selectedVertex.Y < 0)
                    selectedVertex.Y = 0;
                if (selectedVertex.Y > Canvas.Height)
                    selectedVertex.Y = Canvas.Height;
                DrawAll();
            }
        }
        public void SetLanguage(string language)
        {
            var oldCanvasSize = Canvas.Size; 
            var oldSize = Size;
            var cultureInfo = new CultureInfo(language);
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
            Controls.Clear();
            InitializeComponent();
            Size = oldSize;
            if(selectedVertex != null)
                buttonDelVert.Enabled = true;
            button4.Focus();
            Canvas.Size = oldCanvasSize;
            Canvas.Image = drawArea;
            Canvas.Refresh();
            KeyPreview = true;
            label1.BackColor = activeVertexColor;
            if (language.Contains("pl")) button1.Enabled = true;
            if (language.Contains("en")) button2.Enabled = true;
        }
    }
    

    public class Vertex
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Color Color { get; set; }
        public int Index { get; set; }

        public Vertex() { }
        public Vertex(int x, int y, Color color, int index)
        {
            X = x;
            Y = y;
            Color = color;
            Index = index;
        }
    }

    public class Edge
    {
        public Vertex From { get; set; }
        public Vertex To { get; set; }

        public Edge() {}
        public Edge(Vertex from, Vertex to)
        {
            From = from;
            To = to;
        }

    }
    public class Graph
    {
        public List<Vertex> vertices;
        public List<Edge> edges;
        
        public Graph() 
        {
            vertices = new List<Vertex>();
            edges = new List<Edge>();
        }
        public Graph(List<Vertex> v, List<Edge> e)
        {
            edges = e;
            vertices = v;
        }
        public Graph(Graph H)
        {
            vertices = H.vertices;
            edges = H.edges;
        }
    }
}
